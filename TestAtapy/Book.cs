﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    abstract class Book : Product
    {

        public int Pages { get; set; }


        public Book(string name, int price, string barCode, int pages) : base(name, price, barCode)
        {
            if (pages <= 0)
            {
                throw new Exception("Количество страниц не может быть меньше или равным 0");
            }
            else
                this.Pages = pages;
            this.Category = "Book";
        }

        public override void Display()
        {
            Console.WriteLine(new string('-', 24));
            Console.WriteLine(" Категория: {0} \n Подкатегория: {1} \n Название: {2} \n Цена: {3} руб. \n Количество страниц: {4} \n Штрих-код: {5} ",
                 Category, SubCategory, Name, Price, Pages, BarCode);
        }
    }


}
