﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class CD : Disc
    {
        public CD(string name, int price, string barCode, Content content) : base(name, price, barCode, content)
        {
            this.SubCategory = "CD";
        }
    }
}
