﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class CoockingBook : Book
    {
        public string MainIngredient { get; set; }

        public CoockingBook(string name, int price, string barCode, int pages, string mainIngredient) : base(name, price, barCode, pages)
        {
            this.MainIngredient = mainIngredient;
            this.SubCategory = "CoockingBook";
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine(" Основной ингредиент: {0} ", MainIngredient);
        }
    }
}
