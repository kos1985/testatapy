﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class DVD : Disc
    {
        public DVD(string name, int price, string barCode, Content content) : base(name, price, barCode, content)
        {
            this.SubCategory = "DVD";
        }
    }
}
