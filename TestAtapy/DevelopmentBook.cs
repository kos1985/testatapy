﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class DevelopmentBook : Book
    {
        public string DevelopmentLanguage { get; set; }

        public DevelopmentBook(string name, int price, string barCode, int pages, string developmentLanguage) : base(name, price, barCode, pages)
        {
            this.DevelopmentLanguage = developmentLanguage;
            this.SubCategory = "DevelopmentBook";
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine(" Язык программирования: {0} ", DevelopmentLanguage);
        }
    }
}
