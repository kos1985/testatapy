﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{

    enum Content
    {
        Music,
        Video,
        Software
    }

    class Disc : Product
    {
        readonly Content content;

        public Disc(string name, int price, string barCode, Content content) : base(name, price, barCode)
        {
            this.content = content;
            this.Category = "Disk";
        }

        public override void Display()
        {
            Console.WriteLine(new string ('-', 24));
            Console.WriteLine(" Категория: {0} \n Подкатегория: {1} \n Содержание: {2} \n Название: {3} \n Цена: {4} руб. \n Штрих-код: {5} ", 
                Category, SubCategory, content, Name, Price, BarCode);
        }
    }
}
