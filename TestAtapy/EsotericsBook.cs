﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class EsotericsBook : Book
    {

        public int MinAge { get; set; }

        public EsotericsBook(string name, int price, string barCode, int pages, int minAge) : base(name, price, barCode, pages)
        {
            if (minAge <= 0)
            {
                throw new Exception("Возраст не может быть меньше или равен 0");
            }
            else
                this.MinAge = minAge;

            this.SubCategory = "EsotericsBook";
        }

        public override void Display()
        {
            base.Display();
            Console.WriteLine(" Минимальный возраст: {0} ", MinAge);
        }
    }
}
