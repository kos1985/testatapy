﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    abstract class Product
    {

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string BarCode { get; set; }

        public Product(string name, int price, string barCode)
        {
            if (price < 0)
                throw new Exception("Цена не может быть ниже 0");
            if (!long.TryParse(barCode, out long j))
                throw new Exception("Штрих-код должен содержать только цифры");
            this.Name = name;
            this.Price = price;
            this.BarCode = barCode;
        }

        public abstract void Display();

    }
}
