﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAtapy
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = new List<Product>
            {
                new CD("Какой-то диск2", 150, "40909988", Content.Software),
                new DVD("Какой-то диск1", 150, "40909988", Content.Software),
                new DVD("Какой-то диск3", 150, "40909988", Content.Software),
                new CD("Какой-то диск6", 150, "40909988", Content.Software),
                new EsotericsBook("Какая-то книга4", 500, "46548646453", 450, -1),
                new CoockingBook("Какая-то книга3", 500, "46548646453", 450, "Какой-то ингредиент"),
                new DevelopmentBook("Какая-то книга2", 500, "46548646453", 450, "Какой-то язык"),
                new DevelopmentBook("Какая-то книга8", 500, "46548646453", 450, "Какой-то язык"),
                new CoockingBook("Какая-то книга6", 500, "46548646453", 450, "Какой-то ингредиент"),
                new EsotericsBook("Какая-то книга5", 500, "46548646453", 450, 12),
                new CoockingBook("Какая-то книга1", 500, "46548646453", 450, "Какой-то ингредиент"),
                new EsotericsBook("Какая-то книга9", 500, "46548646453", 450, 12),
                new DevelopmentBook("Какая-то книга0", 500, "46548646453", 450, "Какой-то язык")
            };


            var selectedProducts = from p in products orderby p.Category, p.SubCategory select p;

            foreach (var s in selectedProducts)
            {
                s.Display();
            }
            
            //products[0].Display();
            //products[1].Display();

            Console.ReadLine();
        }
    }
}
